const button = document.querySelector("#btn-click")
button.addEventListener('click' , ()=>{
    let p = document.createElement('p')
    p.innerHTML = "New Paragraph"
    button.before(p)
}, {once : true})

const footer = document.querySelector("#content")
let btn2 = document.createElement('button')
btn2.id = "btn-input-create"
btn2.innerHTML = "Click me to add input container"
footer.append(btn2)

btn2.addEventListener("click", ()=>{
    let inp = document.createElement("input")
    inp.type = "email"
    inp.placeholder = "Enter your email"
    inp.name = 'emain-holder'
    inp.style.display = "block"
    inp.style.margin = "auto"
    inp.style.marginTop = "10px"
    btn2.after(inp)
}, {once : true})